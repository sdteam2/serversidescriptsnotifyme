<?php
	 
	/*
	 * Following code will create a new product row
	 * All product details are read from HTTP GET Request
	 */
	include('include_db.php');  
	// array for JSON response
	$response = array();
	require("class.phpmailer.php");
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->CharSet="UTF-8";
	$mail->SMTPSecure = 'tls';
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 587;
	$mail->Username = 'notifyme.group8@gmail.com';
	$mail->Password = 'notify.me';
	$mail->SMTPAuth = true;
	$mail->From = 'notifyme.group8@gmail.com';
	$mail->FromName = 'Team Notify Me';
	$mail->AddReplyTo('notifyme.group8@gmail.com', 'Team Notify Me');
	 
	// check for required fields
	if (isset($_GET['email']))
	{ 
		$email = $_GET['email'];
		$query="SELECT password from student_login where email='$email'";
		$result = mysqli_query($conn,$query);
		// check if row inserted or not
		if ($result) 
		{
			$count=mysqli_num_rows($result);
			if($count==1)
			{
				$row=mysqli_fetch_row($result);
				$password=$row[0];
		
				// successfully inserted into database
				$response["success"] = 1;
				$response["message"] = "Email Sent Successfully.";
				mysqli_close($conn);
				$demail=urldecode($email);
				$mail->AddAddress($demail);  // Add a recipient
				$mail->IsHTML(true);                                  // Set email format to HTML
				echo json_encode($response);
				$mail->Subject = 'Notify Me - Please find your password';
				$mail->Body    = "Hi, your Password is <b> $password </b> ";
				$mail->AltBody = 'This is the body in plain text for non-HTML mail clients ';
				if(!$mail->Send()) 
				{
					echo 'Message could not be sent.';
					echo 'Mailer Error: ' . $mail->ErrorInfo;	
				}
			}
		}	
		else 
		{
			// failed to insert row
			$response["success"] = 0;
			$response["message"] = "Oops! An error occurred.";
			// echoing JSON response
			echo json_encode($response);
		}
		
	}	
	else 
	{
		// required field is missing
		$response["success"] = 0;
		$response["message"] = "Required field(s) is(are) missing";
		// echoing JSON response
		echo json_encode($response);
	}
?>