<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP GET Request
 */
include('include_db.php'); 
// array for JSON response
$response = array();
 
// check for required fields
if (isset($_GET['email'])  && isset($_GET['password']) ) {
 
    $email = $_GET['email'];
    $pass = $_GET['password'];	
	$stmt = $conn->prepare('SELECT fname,lname,user_type from student_login where email=? and password=? and verified_email=?');
	if($stmt!==false){
		$abc=1;
	$stmt->bind_param("ssi",$email,$pass,$abc);
	$stmt->execute();
	$result = $stmt->get_result();
	$r = mysqli_fetch_array($result);
	$row =mysqli_num_rows($result);
	$response["detail"] = array();
	mysqli_close($conn);
    if ($row==1) {
		$detail = array();
		$detail["userType"]=$r["user_type"];
		$detail["fname"] = $r["fname"];
		$detail["lname"] = $r["lname"];
		array_push($response["detail"], $detail);
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Login Sucessfull.";
		echo json_encode($response);
	}
	else
	{
		$response["success"]=2;
		$response["message"]="Account is not verfied please verify your account.";
		echo json_encode($response);
	}		
}
}
 else {
    $response["success"] = 0;
    $response["message"] = "Invalid Request";
    echo json_encode($response);
}
?>