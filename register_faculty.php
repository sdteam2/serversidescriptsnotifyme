<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP GET Request
 */
include('include_db.php');  
// array for JSON response
$response = array();
require("class.phpmailer.php");
$mail = new PHPMailer();
$mail->IsSMTP();
$mail->CharSet="UTF-8";
$mail->SMTPSecure = 'tls';
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->Username = 'notifyme.group8@gmail.com';
$mail->Password = 'notify.me';
$mail->SMTPAuth = true;
$mail->From = 'notifyme.group8@gmail.com';
$mail->FromName = 'Team Notify Me';
$mail->AddReplyTo('notifyme.group8@gmail.com', 'Team Notify Me');
 
// check for required fields
if (isset($_GET['fname']) && isset($_GET['email']) && isset($_GET['lname']) && isset($_GET['password'])) {
 
    $fname = $_GET['fname'];
	$user_type=2;
    $email = $_GET['email'];
	$lname = $_GET['lname'];
    $pass = $_GET['password'];
	$confirm_code = md5(uniqid(rand()));
// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
	$query="INSERT INTO student_login(fname,lname,user_type,email,password,confirm_code) VALUES('$fname', '$lname',$user_type, '$email' ,'$pass','$confirm_code')";
	$result = mysqli_query($conn,$query);
	mysqli_close($conn);
    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "User Registered Successfully.";
		$demail=urldecode($email);
		$mail->AddAddress($demail);  // Add a recipient
		$mail->IsHTML(true);                                  // Set email format to HTML
		echo json_encode($response);
		$mail->Subject = 'Notify Me - Please verify you email address';
		$mail->Body    = "Hi $fname, your Confirmation link is below \r\n 
		Click on this link to activate your account \r\n 
			
			<a href='http://144.118.242.80:7777/notifyme/confirmationfaculty.php?passkey=$confirm_code'>Click here to verify your account</a>";
		$mail->AltBody = 'This is the body in plain text for non-HTML mail clients ';

		if(!$mail->Send()) {
	echo 'Message could not be sent.';
	echo 'Mailer Error: ' . $mail->ErrorInfo;	
    }
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";
 
        // echoing JSON response
        echo json_encode($response);
		
		
	}
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is(are) missing";

    // echoing JSON response
    echo json_encode($response);
}
?>