-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2016 at 04:59 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `notifyme`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE IF NOT EXISTS `announcement` (
`id` int(5) NOT NULL,
  `asubject` varchar(20) COLLATE utf16_bin NOT NULL,
  `abody` longtext COLLATE utf16_bin NOT NULL,
  `f_email` varchar(20) COLLATE utf16_bin NOT NULL,
  `sub_code` varchar(15) COLLATE utf16_bin NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `asubject`, `abody`, `f_email`, `sub_code`, `time`) VALUES
(5, 'No Lecture on 10 Dec', 'Hi class, there will be no class on 10th November as I am going out of town.', 'abc12@drexel.edu', 'CS-575', '2016-10-26 07:24:32'),
(6, 'Online Final Due ', 'Online Students, \r\nYour final exam will be due 11:59pm on Friday the 9th. ', 'abc12@drexel.edu', 'CS-575', '2016-12-05 08:31:20'),
(7, 'Final Video', 'Hi class, you do not have to create the video for privious user stories submitted during midterm demo.', 'abc12@drexel.edu', 'CS-575', '2016-12-05 07:31:47');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_details`
--

CREATE TABLE IF NOT EXISTS `faculty_details` (
  `email` varchar(20) COLLATE utf16_bin NOT NULL,
  `phone` int(15) NOT NULL,
  `office_hours` varchar(50) COLLATE utf16_bin NOT NULL,
  `office_location` varchar(50) COLLATE utf16_bin NOT NULL,
  `subjects` varchar(60) COLLATE utf16_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `faculty_details`
--

INSERT INTO `faculty_details` (`email`, `phone`, `office_hours`, `office_location`, `subjects`) VALUES
('abc12@drexel.edu', 2147483647, '5 pm to 6 pm mondays', 'Ucross Building Room 152', 'CS-345,CS-520,CS-570'),
('abc21@drexel.edu', 2147483647, '9 pm to 10 pm thursdays', 'Rush Building Room 212', 'CS-575,EC-450');

-- --------------------------------------------------------

--
-- Table structure for table `student_details`
--

CREATE TABLE IF NOT EXISTS `student_details` (
  `email` varchar(20) COLLATE utf16_bin NOT NULL,
  `department` varchar(10) COLLATE utf16_bin NOT NULL,
  `subjects` varchar(50) COLLATE utf16_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `student_details`
--

INSERT INTO `student_details` (`email`, `department`, `subjects`) VALUES
('trp64@drexel.edu', 'CCI', 'CS-521,CS-575,CS-520');

-- --------------------------------------------------------

--
-- Table structure for table `student_login`
--

CREATE TABLE IF NOT EXISTS `student_login` (
  `email` char(20) COLLATE utf16_bin NOT NULL,
  `password` char(20) COLLATE utf16_bin NOT NULL,
  `user_type` int(1) NOT NULL,
  `fname` char(20) COLLATE utf16_bin NOT NULL,
  `lname` char(20) COLLATE utf16_bin NOT NULL,
  `verified_email` int(1) NOT NULL,
  `confirm_code` varchar(65) COLLATE utf16_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `student_login`
--

INSERT INTO `student_login` (`email`, `password`, `user_type`, `fname`, `lname`, `verified_email`, `confirm_code`) VALUES
('', '', 1, '', '', 0, 'b897b25c913c24f519ed43184d3737b7'),
('abc12@drexel.edu', 'qwerty', 2, 'Adam', 'Vogues', 1, 'sdhwq126428fkdf274y92febf237721yfn'),
('abc21@drexel.edu', 'qwerty', 2, 'Alexa', 'Morgan', 1, '0a8c24609b17f57a38d08b8eb5ae212c'),
('trp64@drexel.edu', 'qwerty', 1, 'Taral', 'Patel', 1, '860d5ce3c504393c605e321f912020e9');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `sub_code` varchar(10) COLLATE utf16_bin NOT NULL,
  `sub_name` varchar(30) COLLATE utf16_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`sub_code`, `sub_name`) VALUES
('CS-520', 'Artificial Intelligence'),
('CS-521', 'Data Structures and Algorithms'),
('CS-575', 'Software Design');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_details`
--
ALTER TABLE `faculty_details`
 ADD KEY `fk_faculty_email` (`email`);

--
-- Indexes for table `student_details`
--
ALTER TABLE `student_details`
 ADD KEY `fk_student_email` (`email`);

--
-- Indexes for table `student_login`
--
ALTER TABLE `student_login`
 ADD PRIMARY KEY (`email`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
 ADD PRIMARY KEY (`sub_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `faculty_details`
--
ALTER TABLE `faculty_details`
ADD CONSTRAINT `fk_faculty_email` FOREIGN KEY (`email`) REFERENCES `student_login` (`email`);

--
-- Constraints for table `student_details`
--
ALTER TABLE `student_details`
ADD CONSTRAINT `fk_student_email` FOREIGN KEY (`email`) REFERENCES `student_login` (`email`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
