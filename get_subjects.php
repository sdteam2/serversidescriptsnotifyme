<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP GET Request
 */
include('include_db.php'); 
// array for JSON response
$response = array();
 	
	$query = 'SELECT * FROM subjects ';
	$result = mysqli_query($conn,$query);
	$response["detail"] = array();
	$detail = array();
	if (mysqli_num_rows($result) > 0){
	while($row = mysqli_fetch_assoc($result)){
		$detail["sub_code"]=$row["sub_code"];
		$detail["sub_name"] = $row["sub_name"];
		array_push($response["detail"], $detail);
	}		
	
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Sucessfull.";
		echo json_encode($response);
	}
	else{
	mysqli_close($conn);
    $response["success"] = 0;
    $response["message"] = "Invalid Request";
    echo json_encode($response);
	}
	?>