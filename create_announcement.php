<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP GET Request
 */
include('include_db.php');  
// array for JSON response
$response = array();

 
// check for required fields
if (isset($_GET['asubject']) && isset($_GET['abody']) && isset($_GET['atime'])) {
 
    $asubject = $_GET['asubject'];
    $abody = $_GET['abody'];
	$atime = strtotime($_GET['atime']);
	
	$query="INSERT INTO announcement(asubject,abody,time) VALUES('$asubject', '$abody','$atime')";
	$result = mysqli_query($conn,$query);
	mysqli_close($conn);
    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Announcement Posted Successfully.";
    
		// echoing JSON response
        echo json_encode($response);
    }
     else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";
	}
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is(are) missing";

    // echoing JSON response
    echo json_encode($response);
}


?>